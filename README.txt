Retroadmin -- an administrative theme for Drupal

Retroadmin is a small, light, and very fast administration theme for
Drupal 5.x. It is based on the administrative section of Drupal 4.1.

**IMPORTANT NOTE** Retroadmin is *not* a theme for user-facing pages!
Use it *only* for the administrative theme.

To enable retroadmin for your site, complete the following steps:

1. Copy the retroadmin/ directory into the sites/ hierarchy. Either
sites/all/themes for all sites, or sites/<yoursite>/themes for just one.

2. Choose retroadmin in the "Administration theme" select box at
admin/settings/admin. Note that the theme does *not* have to be enabled in
admin/build/themes.

3. Reminisce.
