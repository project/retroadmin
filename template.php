<?php

function _retroadmin_admin_menus() {
  $arg = 0;
  $path = arg($arg);
  while (arg($arg)) {
    $output .= '<div class="admin-menu level-' . $arg . '">';
    $items = array();
    $menu = menu_get_item(NULL, $path);
    if (is_array($menu['children'])) {
      usort($menu['children'], '_menu_sort');
      if ($path == 'admin') {
        $items[] = l(t('home'), '');
      }
    }
    if (is_array($menu['children'])) {
      foreach ($menu['children'] as $mid) {
        $thisitem = menu_get_item($mid);
        $items[] = l($thisitem['title'], $thisitem['path']);
      }
    }
    $output .= implode('&nbsp;|&nbsp;' , $items);
    $output .= '</div><!-- admin-menu -->';
    $output .= '<hr />';
    $arg++;
    $path .= '/' . arg($arg);
  }
  return $output;
}

function retroadmin_admin_block_content($content) {
  if (!$content) {
    return '';
  }

  $output  = '<div class="admin-menu">';
  foreach ($content as $item) {
    $block_links[] = l($item['title'], $item['path'], array('title' => $item['description']));
  }
  $output .= implode('&nbsp;|&nbsp;', $block_links);
  $output .= '</div><!-- admin-menu -->';
  $output .= '<hr />';
  return $output;
}

function retroadmin_admin_page($blocks) {
  foreach ($blocks as $block) {
    $output .= theme('admin_block', $block);
  }
  return $output;
}

function retroadmin_admin_block($block) {
  $output .= '<h2>' . $block['title'] . '</h2>';
  $output .= $block['content'];
  return $output;
}

